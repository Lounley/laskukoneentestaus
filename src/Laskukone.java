public class Laskukone {
    double tulos;

    public Laskukone() {
        nollaa();
    }

    public void lisaaLuku(double luku) {
        tulos += luku;
    }

    public void vahennaLuku(double luku) {
        tulos -= luku;
    }

    public void kerroLuvulla(double luku) {
        double kerrottava = this.annaTulos();
    	for (int i=1; i<luku; i++) {
            lisaaLuku(kerrottava);
        }
    	if (luku<0) {
    		kerrottava = this.annaTulos();
    		this.vahennaLuku(kerrottava);
    		this.vahennaLuku(kerrottava);
    	}
    }

    public void korotaPotenssiin(int luku) {
        double kerrottava = this.annaTulos();

    	for (int i=1; i<luku; i++) {
            kerroLuvulla(kerrottava);
        }
    }

    public double annaTulos() {
        return tulos;
    }

    public void nollaa() {
        tulos = 0;
    }
}