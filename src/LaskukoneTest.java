import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LaskukoneTest {

	@Test
	void lisaaLukuTest() {
		Laskukone laskukone = new Laskukone();
		int lasku = 1+5+8+9;
		
		laskukone.lisaaLuku(1);
		laskukone.lisaaLuku(5);
		laskukone.lisaaLuku(8);
		laskukone.lisaaLuku(9);
		
		assertEquals(lasku, laskukone.annaTulos());
	}
	@Test
	void VahennaLukuTest() {
		Laskukone laskukone = new Laskukone();
		int lasku = 0-1-5-8-9;
		
		laskukone.vahennaLuku(1);
		laskukone.vahennaLuku(5);
		laskukone.vahennaLuku(8);
		laskukone.vahennaLuku(9);
		
		assertEquals(lasku, laskukone.annaTulos());
	}
	
	@Test
	void nollaaTest() {
		Laskukone laskukone = new Laskukone();
		int lasku = 0;
		
		assertEquals(lasku, laskukone.annaTulos());
	}
	
	@Test
	void kerroLuvullaTest() {
		Laskukone laskukone = new Laskukone();
		int lasku = 5*126*-1*-1;
		
		laskukone.lisaaLuku(5);
		
		laskukone.kerroLuvulla(126);
		laskukone.kerroLuvulla(-1);
		laskukone.kerroLuvulla(-1);

		
		assertEquals(lasku, laskukone.annaTulos());
	}
	
	@Test
	void kerroLuvullaTest2() {
		Laskukone laskukone = new Laskukone();
		int lasku = 5*126*-1;
		
		laskukone.lisaaLuku(5);
		
		laskukone.kerroLuvulla(126);
		laskukone.kerroLuvulla(-1);

		
		assertEquals(lasku, laskukone.annaTulos());
	}
	
	@Test void korotaPotenssiinTest() {
		Laskukone laskukone = new Laskukone();
		int lasku = 5*5*5;
		
		laskukone.lisaaLuku(5);
		laskukone.korotaPotenssiin(3);
		
		assertEquals(lasku, laskukone.annaTulos());
	}
}
